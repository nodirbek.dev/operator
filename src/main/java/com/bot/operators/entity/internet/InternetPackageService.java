package com.bot.operators.entity.internet;

import com.bot.operators.dto.base.BaseError;
import com.bot.operators.dto.base.BaseResponse;
import com.bot.operators.util.AppUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class InternetPackageService {
    private final InternetPackageRepository internetPackageRepository;

    public BaseResponse<InternetPackage> insertInternetPackage(InternetPackage internetPackage) {
        try {
            internetPackage.setCreatedAt(AppUtil.now());
            internetPackage.setActive(true);
            InternetPackage insertedInternetPackage = internetPackageRepository.save(internetPackage);
            return BaseResponse.<InternetPackage>builder()
                    .success(true)
                    .data(insertedInternetPackage)
                    .build();
        } catch (Exception e) {
            return BaseResponse.<InternetPackage>builder()
                    .success(false)
                    .baseError(BaseError.builder()
                            .code(-1)
                            .message(e.getLocalizedMessage())
                            .build())
                    .build();
        }
    }
}
