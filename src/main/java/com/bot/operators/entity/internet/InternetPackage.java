package com.bot.operators.entity.internet;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
@Table(name = "internet_packages")
public class InternetPackage {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "category_id")
    private Integer categoryId;
    private String title;
    private Integer amount;
    private String unit;
    private String duration;
    private Integer price;
    private String currency;
    @Column(columnDefinition = "boolean default true")
    private Boolean active;
    @Column(name = "created_at", columnDefinition = "TIMESTAMP")
    private LocalDateTime createdAt;
}
