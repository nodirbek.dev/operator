package com.bot.operators.entity.internet;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InternetPackageRepository extends JpaRepository<InternetPackage, Integer> {

}
