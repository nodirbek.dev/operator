package com.bot.operators.entity.usluga;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UslugaRepository extends JpaRepository<Usluga, Integer> {
    List<Usluga> findByTariffId(Integer tariffId);
}
