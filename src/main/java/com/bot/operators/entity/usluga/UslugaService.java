package com.bot.operators.entity.usluga;

import com.bot.operators.dto.base.BaseError;
import com.bot.operators.dto.base.BaseResponse;
import com.bot.operators.util.AppUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UslugaService {
    private final UslugaRepository uslugaRepository;

    public BaseResponse<Usluga> insertUsluga(Usluga usluga) {
        try {
            usluga.setCreatedAt(AppUtil.now());
            usluga.setActive(true);
            Usluga insertedUsluga = uslugaRepository.save(usluga);
            return BaseResponse.<Usluga>builder()
                    .success(true)
                    .data(insertedUsluga)
                    .build();
        } catch (Exception e) {
            return BaseResponse.<Usluga>builder()
                    .success(false)
                    .baseError(BaseError.builder()
                            .code(-1)
                            .message(e.getLocalizedMessage())
                            .build())
                    .build();
        }
    }

    public List<Usluga> getUslugaByTariffId(Integer tariffId) {
        return uslugaRepository.findByTariffId(tariffId);
    }
}
