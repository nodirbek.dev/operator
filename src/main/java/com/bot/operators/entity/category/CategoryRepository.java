package com.bot.operators.entity.category;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
    List<Category> findByOperatorIdAndActiveTrue(Integer operatorId);
    List<Category> findByOperatorNameAndActiveTrue(String operatorName);
}
