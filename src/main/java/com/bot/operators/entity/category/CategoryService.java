package com.bot.operators.entity.category;

import com.bot.operators.dto.base.BaseError;
import com.bot.operators.dto.base.BaseResponse;
import com.bot.operators.entity.operator.Operator;
import com.bot.operators.util.AppUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public BaseResponse<Category> insertCategory(Category category) {
        try {
            category.setCreatedAt(AppUtil.now());
            category.setActive(true);
            Category insertedCategory = categoryRepository.save(category);
            return BaseResponse.<Category>builder()
                    .data(insertedCategory)
                    .success(true)
                    .build();
        } catch (Exception e) {
            return BaseResponse.<Category>builder()
                    .success(false)
                    .baseError(BaseError.builder()
                            .code(-1)
                            .message(e.getLocalizedMessage())
                            .build())
                    .build();
        }
    }

    public List<String> getCategoryTitles(Integer operatorId) {
        List<Category> categories = categoryRepository.findByOperatorIdAndActiveTrue(operatorId);
        return categories.stream()
                .map(Category::getTitle)
                .collect(Collectors.toList());
    }

    public List<Category> getCategoriesByOperatorName(String operatorName) {
        return categoryRepository.findByOperatorNameAndActiveTrue(operatorName);
    }
}
