package com.bot.operators.entity.category;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "categories")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "operator_id")
    private Integer operatorId;
    @Column(name = "operator_name")
    private String operatorName;
    private String title;
    @Column(name = "internet_package", columnDefinition = "boolean default false")
    private Boolean internetPackage;
    @Column(name = "created_at", columnDefinition = "TIMESTAMP")
    private LocalDateTime createdAt;
    @Column(columnDefinition = "boolean default true")
    private Boolean active;
}
