package com.bot.operators.entity.tarif;

import com.bot.operators.dto.base.BaseError;
import com.bot.operators.dto.base.BaseResponse;
import com.bot.operators.util.AppUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TariffService {
    private final TariffRepository tariffRepository;

    public BaseResponse<Tariff> insertTariff(Tariff tariff) {
        try {
            tariff.setCreatedAt(AppUtil.now());
            tariff.setActive(true);
            Tariff insertedTariff = tariffRepository.save(tariff);
            return BaseResponse.<Tariff>builder()
                    .success(true)
                    .data(insertedTariff)
                    .build();
        } catch (Exception e) {
            return BaseResponse.<Tariff>builder()
                    .success(false)
                    .baseError(BaseError.builder()
                            .code(-1)
                            .message(e.getLocalizedMessage())
                            .build())
                    .build();
        }
    }

    public List<Tariff> getAllTariffByCategoryAndOperator(String operatorName, String categoryName) {
        return tariffRepository.findByOperatorNameAndCategoryNameAndActiveTrue(operatorName, categoryName);
    }

    public Tariff getTariffByOperatorNameAndTitle(String operatorName, String title) {
        return tariffRepository.findByOperatorNameAndTitleAndActiveTrue(operatorName, title);
    }
}
