package com.bot.operators.entity.tarif;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
@Table(name = "tariffs")
public class Tariff {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "operator_id")
    private Integer operatorId;
    @Column(name = "operator_name")
    private String operatorName;
    @Column(name = "category_id")
    private Integer categoryId;
    @Column(name = "category_name")
    private String categoryName;
    private String title;
    @Column(name = "operator_phone")
    private String operatorPhone;
    private Integer price;
    @Column(name = "site_url")
    private String siteUrl;
    private String currency;
    @Column(name = "ussd_code")
    private String ussdCode;
    @Column(columnDefinition = "boolean default true")
    private Boolean active;
    @Column(name = "created_at", columnDefinition = "TIMESTAMP")
    private LocalDateTime createdAt;
}
