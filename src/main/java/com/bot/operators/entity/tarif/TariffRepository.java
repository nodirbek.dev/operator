package com.bot.operators.entity.tarif;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TariffRepository extends JpaRepository<Tariff, Integer> {
    List<Tariff> findByOperatorNameAndCategoryNameAndActiveTrue(String operatorName, String categoryName);

    Tariff findByOperatorNameAndTitleAndActiveTrue(String operatorName, String title);
}
