package com.bot.operators.entity.operator;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface OperatorRepository extends JpaRepository<Operator, Integer> {
    Operator findByTitleAndActiveTrue(String title);

    List<Operator> findByActiveTrue();
}
