package com.bot.operators.entity.operator;

import com.bot.operators.dto.base.BaseError;
import com.bot.operators.dto.base.BaseResponse;
import com.bot.operators.entity.user.User;
import com.bot.operators.entity.user.UserService;
import com.bot.operators.util.AppUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OperatorService {
    private final OperatorRepository operatorRepository;
    private final UserService userService;

    public BaseResponse<Operator> insertOperator(Operator operator) {
        try {
            operator.setCreatedAt(AppUtil.now());
            operator.setActive(true);
            Operator savedOperator = operatorRepository.save(operator);
            return BaseResponse.<Operator>builder()
                    .success(true)
                    .data(savedOperator)
                    .build();
        } catch (Exception e) {
            return BaseResponse.<Operator>builder()
                    .success(false)
                    .baseError(BaseError.builder()
                            .code(-1)
                            .message(e.getLocalizedMessage())
                            .build())
                    .build();
        }
    }

    public List<String> getAllActiveOperators() {
        List<Operator> operators = operatorRepository.findByActiveTrue();
        return operators.stream()
                .map(Operator::getTitle)
                .collect(Collectors.toList());
    }

    public Integer getOperatorIdByTitle(String title, String chatId) {
        Operator operator = operatorRepository.findByTitleAndActiveTrue(title);
        User user = userService.getUserByChatId(chatId);
        user.setMenuId(operator.getId());
        user.setMenu(title);
        userService.insertUser(user);
        return operator.getId();
    }
}
