package com.bot.operators.entity.user;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
@Table(name = "users")
public class User {
    @Id
    @Column(name = "chat_id", unique = true)
    private String chatId;
    private String username;
    private String firstname;
    private String lastname;
    private String phone;
    @Column(name = "menu_id")
    private Integer menuId;
    private String operator;
    @Column(name = "menu")
    private String menu;
    private Boolean active;
    @Column(name = "created_at", columnDefinition = "TIMESTAMP")
    private LocalDateTime createdAt;
}
