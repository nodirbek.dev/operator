package com.bot.operators.entity.user;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public void insertUser(User user) {
        userRepository.save(user);
    }

    public User getUserByChatId(String chatId) {
        return userRepository.findByChatId(chatId);
    }

}
