package com.bot.operators.dto;

import com.bot.operators.enums.DataType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class DataHolder {
    private DataType type;
    private String text;
    private List<String> list;
    private String imageUrl;
}
