package com.bot.operators.dto.ucell;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class UcellResultDto {
    private String title;
    private List<UcellResElementDto> elements;
}
