package com.bot.operators.dto.ucell;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class UcellResElementDto {
    private String service;
    private String def;
}
