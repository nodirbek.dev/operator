package com.bot.operators.controller;

import com.bot.operators.service.bot.RegisterTelegramBot;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/bot")
public class BotController {
    private final RegisterTelegramBot registerTelegramBot;

    @GetMapping("/start")
    public String startBot() {
        // RegisterTelegramBot telegramBot = new RegisterTelegramBot();
        try {
            TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
            botsApi.registerBot(registerTelegramBot);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
        return "started";
    }
}
