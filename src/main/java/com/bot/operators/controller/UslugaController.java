package com.bot.operators.controller;

import com.bot.operators.entity.usluga.Usluga;
import com.bot.operators.entity.usluga.UslugaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/usluga")
public class UslugaController {
    private final UslugaService uslugaService;

    @PostMapping("/insert")
    public ResponseEntity<?> insertUsluga(@RequestBody Usluga usluga) {
        return ResponseEntity.ok(uslugaService.insertUsluga(usluga));
    }
}
