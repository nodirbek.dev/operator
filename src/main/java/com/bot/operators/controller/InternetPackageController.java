package com.bot.operators.controller;

import com.bot.operators.entity.internet.InternetPackage;
import com.bot.operators.entity.internet.InternetPackageService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/internet/package")
public class InternetPackageController {
    private final InternetPackageService internetPackageService;

    @PostMapping("/insert")
    public ResponseEntity<?> insertInternetPackage(@RequestBody InternetPackage internetPackage) {
        return ResponseEntity.ok(internetPackageService.insertInternetPackage(internetPackage));
    }
}
