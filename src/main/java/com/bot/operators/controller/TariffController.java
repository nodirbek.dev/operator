package com.bot.operators.controller;

import com.bot.operators.entity.tarif.Tariff;
import com.bot.operators.entity.tarif.TariffService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/tariff")
public class TariffController {
    private final TariffService tariffService;
    @PostMapping("/insert")
    public ResponseEntity<?> insertTariff(@RequestBody Tariff tariff) {
        return ResponseEntity.ok(tariffService.insertTariff(tariff));
    }
}
