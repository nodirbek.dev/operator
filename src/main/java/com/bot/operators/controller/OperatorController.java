package com.bot.operators.controller;

import com.bot.operators.entity.operator.Operator;
import com.bot.operators.entity.operator.OperatorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/operator")
public class OperatorController {
    private final OperatorService operatorService;

    @PostMapping("/insert")
    public ResponseEntity<?> insertOperator(@RequestBody Operator operator) {
        return ResponseEntity.ok(operatorService.insertOperator(operator));
    }

    @GetMapping("/getAllActive")
    public ResponseEntity<?> insertOperator() {
        return ResponseEntity.ok(operatorService.getAllActiveOperators());
    }
}
