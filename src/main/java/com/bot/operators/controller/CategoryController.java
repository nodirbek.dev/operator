package com.bot.operators.controller;

import com.bot.operators.entity.category.Category;
import com.bot.operators.entity.category.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/category")
public class CategoryController {
    private final CategoryService categoryService;

    @PostMapping("/insert")
    public ResponseEntity<?> insertCategory(@RequestBody Category category) {
        return ResponseEntity.ok(categoryService.insertCategory(category));
    }

    @GetMapping("/getByOperatorId")
    public ResponseEntity<?> insertCategory(@RequestParam Integer operatorId) {
        return ResponseEntity.ok(categoryService.getCategoryTitles(operatorId));
    }
}
