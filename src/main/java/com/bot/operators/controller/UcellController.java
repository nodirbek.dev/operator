package com.bot.operators.controller;

import com.bot.operators.dto.ucell.UcellResultDto;
import com.bot.operators.service.html.UcellHtmlReaderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/ucell")
public class UcellController {
    private final UcellHtmlReaderService ucellHtmlReaderService;

    @GetMapping("/services")
    public ResponseEntity<?> getServices() {
        List<UcellResultDto> list = ucellHtmlReaderService.readUcellCite();
        return ResponseEntity.ok(list);
    }

    @GetMapping("/test")
    public String getText() {
        return "hello";
    }
}
