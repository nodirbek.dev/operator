package com.bot.operators.service.html;

import com.bot.operators.dto.ucell.UcellResElementDto;
import com.bot.operators.dto.ucell.UcellResultDto;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class UcellHtmlReaderServiceImpl implements UcellHtmlReaderService {
    @Value("${url.ucell}")
    private String ucellUrl;

    @Override
    public List<UcellResultDto> readUcellCite() {
        List<UcellResultDto> ucellResultDtos = new ArrayList<>();
        try {
            Document doc = Jsoup.connect(ucellUrl).get();
            Elements elements = doc.getElementsByClass("press_contact_dtl");
            for (Element element : elements) {
                Elements strongs = element.select("strong");
                Elements spans = element.select("span");
                String title = strongs.get(0).text();
                List<UcellResElementDto> ucellResElementDtoList = new ArrayList<>();
                for (int i = 0; i < spans.size(); i++) {
                    try {
                        String service = spans.get(i).text();
                        String def = spans.get(i + 1).text();
                        UcellResElementDto ucellResElementDto = UcellResElementDto.builder()
                                .service(service)
                                .def(def)
                                .build();
                        i++;
                        ucellResElementDtoList.add(ucellResElementDto);
                    } catch (Exception e) {
                        System.out.println(e.getLocalizedMessage());
                    }
                }

                ucellResultDtos.add(UcellResultDto.builder()
                        .elements(ucellResElementDtoList)
                        .title(title)
                        .build());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println("size " + ucellResultDtos.size());
        return ucellResultDtos;
    }
}
