package com.bot.operators.service.html;

import com.bot.operators.dto.ucell.UcellResultDto;

import java.util.List;

public interface UcellHtmlReaderService {
    List<UcellResultDto> readUcellCite();
}
