package com.bot.operators.service.bot;

import com.bot.operators.dto.DataHolder;
import com.bot.operators.entity.category.Category;
import com.bot.operators.entity.category.CategoryService;
import com.bot.operators.entity.operator.OperatorService;
import com.bot.operators.entity.tarif.Tariff;
import com.bot.operators.entity.tarif.TariffService;
import com.bot.operators.entity.user.User;
import com.bot.operators.entity.user.UserService;
import com.bot.operators.entity.usluga.Usluga;
import com.bot.operators.entity.usluga.UslugaService;
import com.bot.operators.enums.DataType;
import com.bot.operators.enums.Menus;
import com.vdurmont.emoji.EmojiParser;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LogicService {

    private final UserService userService;
    private final CategoryService categoryService;
    private final TariffService tariffService;
    private final OperatorService operatorService;
    private final UslugaService uslugaService;

    public DataHolder determineMenu(String chatId, String input) {
        User user = userService.getUserByChatId(chatId);
        //List<String> list = new ArrayList<>();
        DataHolder dataHolder = new DataHolder();
        String menu = user.getMenu();
        if (menu.equals(Menus.OPERATOR.name())) {
            dataHolder = DataHolder.builder()
                    .type(DataType.LIST)
                    .list(getCategories(user, input))
                    .build();
        } else if (menu.equals(Menus.CATEGORY.name())) {
            dataHolder = DataHolder.builder()
                    .type(DataType.LIST)
                    .list(getTariffs(user, input))
                    .build();
        } else if (menu.equals(Menus.TARIFF.name())) {
            String text = getTariffUsluga(user, input);
            dataHolder = DataHolder.builder()
                    .type(DataType.TEXT)
                    .text(text)
                    .build();
        }
        return dataHolder;
    }

    public List<String> backIsPressed(String chatId) {
        User user = userService.getUserByChatId(chatId);
        List<String> list = new ArrayList<>();
        String menu = user.getMenu();
        String operator = user.getOperator();
        if (menu.equals(Menus.TARIFF.name())) {
            user.setMenu(Menus.CATEGORY.name());
            userService.insertUser(user);
            list = getCategories(user, operator);
        } else if (menu.equals(Menus.CATEGORY.name())) {
            user.setMenu(Menus.OPERATOR.name());
            userService.insertUser(user);
            list = operatorService.getAllActiveOperators();
        }
        return list;
    }

    public List<String> homePressed(String chatId) {
        User user = userService.getUserByChatId(chatId);
        user.setMenu(Menus.OPERATOR.name());
        userService.insertUser(user);
        return operatorService.getAllActiveOperators();
    }

    private String getTariffUsluga(User user, String tariffTitle) {
        String operator = user.getOperator();
        Tariff tariff = tariffService.getTariffByOperatorNameAndTitle(operator, tariffTitle);
        Integer tariffId = tariff.getId();
        List<Usluga> uslugaList = uslugaService.getUslugaByTariffId(tariffId);
        List<String> messages = new ArrayList<>();
        for (Usluga usluga : uslugaList) {
            String dbEmoji = usluga.getEmoji();
            String message = "";
            if (dbEmoji != null) {
                String emoji = EmojiParser.parseToUnicode(dbEmoji);
                message = message + emoji+" ";
            }
            message = message + usluga.getService() + " " + usluga.getAmount() + " " + usluga.getUnit() + " " + usluga.getAdditional();
            messages.add(message);
        }
        StringBuilder text = new StringBuilder(" ");
        for (String t : messages) {
            text.append(t).append("\n");
        }
        return text.toString();
    }

    private List<String> getTariffs(User user, String category) {
        user.setMenu(Menus.TARIFF.name());
        userService.insertUser(user);
        List<Tariff> tariffs = tariffService.getAllTariffByCategoryAndOperator(user.getOperator(), category);
        return tariffs.stream()
                .map(Tariff::getTitle)
                .collect(Collectors.toList());
    }

    private List<String> getCategories(User user, String operator) {
        user.setMenu(Menus.CATEGORY.name());
        user.setOperator(operator);
        userService.insertUser(user);
        List<Category> categories = categoryService.getCategoriesByOperatorName(operator);
        return categories.stream()
                .map(Category::getTitle)
                .collect(Collectors.toList());
    }
}
