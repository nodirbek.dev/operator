package com.bot.operators.service.bot;


import com.bot.operators.dto.DataHolder;
import com.bot.operators.entity.operator.OperatorService;
import com.bot.operators.entity.user.User;
import com.bot.operators.entity.user.UserService;
import com.bot.operators.enums.DataType;
import com.bot.operators.enums.Menus;
import com.bot.operators.util.AppUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RegisterTelegramBot extends TelegramLongPollingBot {
    private final UserService userService;
    private final OperatorService operatorService;
    private final LogicService logicService;

    @Override
    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();
        Chat chat = message.getChat();
        String messageText = message.getText();
        String chatId = chat.getId().toString();
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(messageText);
        System.out.println(messageText);
        try {
            //Inputs input = Inputs.getEnum(messageText);
            switch (messageText) {
                case "/start" -> {
                    String firstname = chat.getFirstName();
                    String lastname = chat.getLastName();
                    String username = chat.getUserName();
                    createNewUser(firstname, lastname, username, chatId);
                    List<String> operators = operatorService.getAllActiveOperators();
                    sendMessage.setReplyMarkup(makeKeyBoard(operators));
                }
                case "/stop" -> {
                    System.out.println("STOP IS PRESSED");
                }
                case "Orqaga" -> {
                    //System.out.println("orqaga");
                    List<String> list = logicService.backIsPressed(chatId);
                    sendMessage.setReplyMarkup(makeKeyBoard(list));
                }
                case "Home" -> {
                    List<String> operators = logicService.homePressed(chatId);
                    sendMessage.setReplyMarkup(makeKeyBoard(operators));
                }
                default -> {
                    System.out.println("another text came");
                    DataHolder dataHolder = logicService.determineMenu(chatId, messageText);
                    DataType type = dataHolder.getType();
                    switch (type) {
                        case LIST -> {
                            sendMessage.setReplyMarkup(makeKeyBoard(dataHolder.getList()));
                        }
                        case TEXT -> {
                            sendMessage.setText(dataHolder.getText());
                        }
                        case IMAGE -> {
                            sendMessage.setText(dataHolder.getImageUrl());
                        }
                    }
                }
            }
            execute(sendMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void createNewUser(String firstname, String lastname, String username, String chatId) {
        User user = User.builder()
                .chatId(chatId)
                .firstname(firstname)
                .lastname(lastname)
                .username(username)
                .createdAt(AppUtil.now())
                .active(true)
                .menu(Menus.OPERATOR.name())
                .menuId(0)
                .build();
        userService.insertUser(user);
    }

    private ReplyKeyboardMarkup makeKeyBoard(List<String> texts) {
        List<KeyboardRow> keyboardRows = new ArrayList<>();
        for (int i = 0; i < texts.size(); i++) {
            KeyboardRow row = new KeyboardRow();
            row.add(texts.get(i));
            if (i + 1 < texts.size())
                row.add(texts.get(i + 1));
            i++;
            keyboardRows.add(row);
        }
        KeyboardRow row = new KeyboardRow();
        row.add("Orqaga");
        row.add("Home");
        keyboardRows.add(row);
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup(keyboardRows);
        replyKeyboardMarkup.setResizeKeyboard(true);
        return replyKeyboardMarkup;
    }

    @Override
    public String getBotUsername() {
        return "@OoperatorrBot";
    }

    @Override
    public String getBotToken() {
        return "6980236916:AAEEoY-XgT0sRrscR7Wn4sL9ii-bg_KowwQ";
    }
}
