package com.bot.operators;

import com.bot.operators.service.bot.RegisterTelegramBot;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

@SpringBootApplication
public class OperatorsApplication {
    public static void main(String[] args) {
        SpringApplication.run(OperatorsApplication.class, args);
    }
}
