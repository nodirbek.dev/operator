package com.bot.operators.enums;

public enum Menus {
    OPERATOR,
    CATEGORY,
    TARIFF,
    INTERNET
}
