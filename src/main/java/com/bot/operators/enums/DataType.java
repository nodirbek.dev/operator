package com.bot.operators.enums;

public enum DataType {
    TEXT,
    LIST,
    IMAGE
}
