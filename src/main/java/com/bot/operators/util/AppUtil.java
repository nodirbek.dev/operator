package com.bot.operators.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class AppUtil {
    public static LocalDateTime now() {
        Instant instant = Instant.now();
        ZonedDateTime currentISTime = instant.atZone(ZoneId.of("Asia/Tashkent"));
        return currentISTime.toLocalDateTime();
    }
}
